# Barista Assignment

## Introduction

The following repository contains the source code for the Barista assignment.  The repository is broken down into three sub-folders; *api, provision, and ui*.  Consult each sub-folder for information specifically related to that folder's content including how to build.

**The *api* Folder:**

This folder contains Barista APIs, the API contract (OpenAPI), the build system to create the Barista API Docker image, etc.

**The *provision* Folder:**

This folder contains Terraform modules and a plan to create the foundation AWS infrastructure.

**The *ui* Folder:**

The *ui* folder contains the Gatsby project for the Barista web interface for both the Coffeehouses as well as the Customers to manage their accounts, the orders, etc.

## Context

This is a take home coding test to demonstrate capabilities in the development and operations space.

The task is described in the user story and acceptance criteria below. Implementation details such as language, framework and API contract are up to the engineer.

The code should be shared online in a way which allows access to a validating employee (ideally in a git repository).

Instructions should be provided on how to build and run the application. It will be built, run and validated on the workstation of the test validator (so no shared deployment or code is necessary).
The test validator will be another software engineer and will look into the functional code, the build system and everything else.

Commit and share whatever you have at the end of the allotted timeframe, don't worry if you didn't fully complete the task.

For the purposes of this test the implementation should be scoped to an example back end API that would enable the desired story for a front end, not a full implementation.

## Requirements

The following are the core requirements to build and deploy the Barista's API and UI:

* Docker

In order to run natively in your sandboxes, you can install the following:

* Node
* GNU Make
* Terraform

## User Story

As a Barista, I want a way for my customers to submit their orders via an app before they arrive so that I can make my customers orders just before they arrive.

**Acceptence Criteria**

* Baristas can register their coffee shop
* Customers can submit their name, order type and time of arrival to a specific coffee shop
* Baristas can see the orders organized by pickup time for their coffee shop

**Non-functional Requirements**

* The code should be the type of code you would consider production ready and could be supported by a team. Write the sort of code you would want to receive from another engineer.
* The application must have a build system
* The application build should be built or compiled in a docker container, so the build is portable
* The application build should produce a docker container as an artifact, so the deployment is portable
* The application should not have any runtime dependencies (so the datastore needs to be in memory or similar)


