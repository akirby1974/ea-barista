#!/bin/bash

# source the common.rc file to set commonly used variables
SCRIPTS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source $SCRIPTS/common.rc

if [ "$BARISTA_CONT_ID" = "" ]; then
    docker build -t $BARISTA_DOCKER_IMAGE . --build-arg BUILD_ENV=$NODE_ENV
else
    echo "Stop the runnning container: $BARISTA_CONT_ID"
fi
