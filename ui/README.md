# Barista Assignment

## Introduction

The content within this section of the repository is just the initial Gatsby Javascript framework and where the Coffeehouse website UI content would reside.

## Requirements

At minimum you will need to install the following:

* Docker

If you want to run this **natively** in your sandbox, you will need the following as well:

* Node
* GNU Make *(only to use make to automate build/deploy)*

## Development

This section will walk you through standing up the Web UI either natively in your sandbox or as a Docker container.

### Standup Native Sandbox

Before you do start up the Gatsby server, you need to run the following command once each time you setup a new sandbox:

```
$> npm install
```

This section walks you through the steps to bring up the Coffeehouse's web UI natively on your machine.

```
$> npm start
```

### Standup Container Sandbox

A Makefile has been created at the root of the API folder that automates a lot of this work for you.  If you have Make installed on your machine, you will have the following commands available to you.  If you don't have Make installed, you can skip this **Standup Container with Make** section and scroll down to **Standup Container with Script** or down to **Standup Container Manually**.

Once the container is running, you should be able to point your browser to the following URL:

```
http://localhost:8080/
```

**Standup Container with Make:**

The following section assumes you have Make installed to automate the building and clean up of the container builds as well as standing them up locally on your machine.  The following table lists the commands available and a description of each:

| Make Command | Description |
|:-------------|:------------|
| build        | Builds the Docker image containing the Web UI source code.   |
| clean        | Removes any previously built Docker images.               |
| start        | Starts the latest built Docker image.                     |
| stop         | Stops the prunes the container from your environment.     |
| attach       | Logs you into the running container through SSH.          |

To get things stood up, run the following:

```
$> make build
$> make start
```

To clean things up, run the following:

```
$> make clean
```

This will stop any running containers, before cleaning up the container and the image from your machine.

If you want to checkout things within your container, you can run the following:

```
$> make attach
```

**NOTE:** The other commands; setup, migrate and seed are not fully implemented yet and at the time of writing this would only be used for setting up the database for a natively stood up build.

**Standup Container with Scripts:**

If you don't want to install Make on your machine, you can use the scripts under the `scripts/` folder.  The following table describes each script:

| Script Name | Description |
|:------------|:------------|
| build.sh    | Builds the Docker image.                               |
| clean.sh    | Removes any traces of the Docker image and containers. |
| start.sh    | Starts an already built Docker image.                  |
| stop.sh     | Stops any running Docker containers.                   |

The above scripts are fairly straightforward; use the `build.sh` to build and re-build the Docker image, use the `clean.sh` to clean up your environment from previous build and runs of the Docker image, use `start.sh` get start the Docker container and use `stop.sh` to stop any running Docker containers.

**Standup Container Manually:**

If you don't want to use the automation in place and prefer more manual way of doing things, you can follow the steps in this section to build, deploy and teardown a locally running container.

To build the Docker image, run the following:

```
$> docker build -t $USER/barista_web_ui_image .
```

To start an already built image:

```
$> docker run -d --name barista_web_ui -p 8080:8080 $USER/barista_web_ui_image:latest
```

If you want to mount your local sandbox as a volume in the running container, run this command instead of the above:

```
$> docker run -d --name barista_web_ui -p 8080:8080 -v $PWD:/app $USER/barista_web_ui_image:latest
```

To stop and clean things up:

```
$> docker stop barista_web_ui
$> docker container prune -f
$> docker image rm -f $USER/barista_web_ui_image
```
