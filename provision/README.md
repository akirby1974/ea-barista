# Barista Assignment


This folder is broken down into two sections; modules and plans.  The modules are shared code to simplify the writing of plans.  Plans are the actual IaC project file(s).

This is not completed, but you can get an overall idea of where things are heading.  The plan is creating a complete infrasutructre with VPC, NAT gateways, public and private subnets, separation between app, database and public faces instances.  What is missing are the load balances, auto-scaling groups and launch configurations.
