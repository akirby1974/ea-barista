
variable "region" {
    description = "The AWS region to deploy into."
    default     = "us-west-2"
}

variable "stack" {
	description = "The stack name to tag all the resources with."
	default     = "demo"
}

variable "azs" {
    description = "A list of Available Zones to deploy into."
    type        = "list"
    default     = [ "us-west-2a", "us-west-2c" ]
}

variable "cidr_block" {
    description = "The base CIDR block to deploy the VPC and resources with."
    default     = "10.5.0.0/16"
}
