
module "rds" {
    source         = "../modules/rds"

    stack          = var.stack
    engine         = "postgres"
    engine_version = "9.6"

    db_subnets     = module.subnet_private_db.subnet_ids
}
