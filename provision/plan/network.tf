
/* create the vpc */
module "vpc" {
    source     = "../modules/vpc"

    stack      = var.stack
    cidr_block = var.cidr_block
}

module "subnet_public" {
    source         = "../modules/subnet/public"

    stack          = module.vpc.stack
    azs            = var.azs
    vpc_id         = module.vpc.vpc_id
    vpc_cidr_block = module.vpc.cidr_block
    route_table_id = module.vpc.default_route_table_id
    network_acl_id = module.vpc.default_network_acl_id
    network_acl_rules = [{
        egress = false
        action = "allow"
    }, {
        egress = true
        action = "allow"
    }]
}

module "nat_gateway" {
    source = "../modules/nat"

    stack = module.vpc.stack
    azs   = var.azs
    public_subnet_ids = module.subnet_public.subnet_ids
}

module "rtb_private" {
    source = "../modules/rtb/private"

    purpose = "app_db"
	stack   = module.vpc.stack
	vpc_id  = module.vpc.vpc_id
	azs     = var.azs
	nat_ids = module.nat_gateway.nat_gateway_ids
}

module "subnet_private_app" {
	source            = "../modules/subnet/private"
	
	purpose           = "app"
	stack             = module.vpc.stack
	azs               = var.azs
	vpc_id            = module.vpc.vpc_id
	vpc_cidr_block    = module.vpc.cidr_block
	cidr_offset       = 1
	route_table_ids   = module.rtb_private.rtb_ids
	network_acl_rules = [{
		action    = "allow"
		protocol  = "tcp"
		from_port = 80
		to_port   = 80
	}, {
		action    = "allow"
		protocol  = "tcp"
		rule_no   = 101
		from_port = 8080
		to_port   = 8080
	}, {
		action    = "allow"
		protocol  = "tcp"
		rule_no   = 102
		from_port = 443
		to_port   = 443
	}, {
		egress    = true
		action    = "allow"
	}]
}

module "subnet_private_db" {
	source            = "../modules/subnet/private"
	
	purpose           = "db"
	stack             = module.vpc.stack
	azs               = var.azs
	vpc_id            = module.vpc.vpc_id
	vpc_cidr_block    = module.vpc.cidr_block
	cidr_offset       = 2
	route_table_ids   = module.rtb_private.rtb_ids
	network_acl_rules = [{
		action    = "allow"
		protocol  = "tcp"
		from_port = 3306
		to_port   = 3306
	}, {
		egress    = true
		action    = "allow"
	}]
}
