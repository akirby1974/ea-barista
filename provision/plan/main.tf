
/* setup the provider */
provider "aws" {
    region = var.region
}

/* define the remote storage for the Terraform state file */
# terraform {
#     backend "s3" {
#         bucket  = "tfstate.s3.development.barista.com"
#         key     = "demo/us-west-2"
#         region  = "us-west-2"
#         encrypt = true
#     }
# }