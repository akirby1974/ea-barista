
/* create the VPC */
resource "aws_vpc" "module_vpc" {
	cidr_block = var.cidr_block
	
	lifecycle {
		create_before_destroy = true
	}
	
	tags = {
		Name  = "${ lower(var.stack) }.vpc }"
		Group = lower(var.stack)
	}
}

/* update the default VPC DHCP options */
resource "aws_default_vpc_dhcp_options" "module_default_dhcp_options" {
	tags = {
		Name  = "${ lower(var.stack) }.vpcdhcp.default }"
		Group = lower(var.stack)
	}
}
