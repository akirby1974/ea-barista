
output "stack" {
	value = var.stack
}

output "vpc_id" {
	value = aws_vpc.module_vpc.id
}

output "cidr_block" {
	value = aws_vpc.module_vpc.cidr_block
}

output "default_route_table_id" {
	value = aws_vpc.module_vpc.default_route_table_id
}

output "default_network_acl_id" {
	value = aws_vpc.module_vpc.default_network_acl_id
}
