
/* Module variables */
variable "stack" {
	description = "The stack name to tag the VPC with."
}

variable "cidr_block" {
	description = "The CIDR block to use when creating the VPC."
}
