
resource "aws_db_instance" "module_rds_db" {
	allocated_storage    = var.allocated_storage
	storage_type         = var.storage_type
	engine               = var.engine
	engine_version       = var.engine_version
	instance_class       = var.instance_class
	name                 = var.db_name
	username             = var.db_username
	password             = var.db_password

	tags = {
		Name  = "${ lower(var.stack) }.rds.${ lower(var.db_name) }"
		Group = lower(var.stack)
	}
}

resource "aws_db_subnet_group" "module_db_subnet" {
	count = var.has_subnets ? 1 : 0

	description = "Database subnet group for ${ var.stack }"
	subnet_ids  = var.db_subnets

	tags = {
		Name  = "${ lower(var.stack) }.rds_subnet.${ lower(var.db_name) }"
		Group = lower(var.stack)
	}
}