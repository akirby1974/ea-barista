output "id" {
	value = aws_db_instance.module_rds_db.id
}

output "endpoint" {
	value = aws_db_instance.module_rds_db.endpoint
}