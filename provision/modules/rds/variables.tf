
/* Module variables */
variable "stack" {
	description = "The stack name to tag the VPC with."
}

variable "allocated_storage" {
	description = "The amount to allocate for storage."
	default     = 20
}

variable "storage_type" {
	description = ""
	default     = "gs2"
}

variable "engine" {
	description = ""
}

variable "engine_version" {
	description = ""
}

variable "instance_class" {
	description = "The instance type to deploy the database on to."
	default     = "db.t2.micro"
}

variable "db_name" {
	description = "The name of the database to be created."
	default     = "demo"
}

variable "db_username" {
	description = "The username to assign to the database."
	default     = "root"
}

variable "db_password" {
	description = "The password to assign to the database."
	default     = ""
}

variable "has_subnets" {
	description = "Associate the RDS with subnets."
	type        = bool
	default     = false
}

variable "db_subnets" {
	description = "The subnet IDs to associate with the rds database."
	type        = list(string)
	default     = []
}