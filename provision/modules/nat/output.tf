
/* Return the following values to the calling resource */
output "nat_gateway_ids" {
	value = aws_nat_gateway.module_nat.*.id
}
