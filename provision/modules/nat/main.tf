
/* Create the Elastic IPs */
resource "aws_eip" "module_eip" {
	count = length(var.azs)
	vpc   = true
	
	lifecycle {
		create_before_destroy = true
	}
}

/* Create the NAT gateways */
resource "aws_nat_gateway" "module_nat" {
	count = length(var.azs)
	
	allocation_id = element(aws_eip.module_eip.*.id, count.index)
	subnet_id     = element(var.public_subnet_ids, count.index)
	
	lifecycle {
		create_before_destroy = true
	}
	
	tags = {
		Name  = "${ var.stack }.nat.${ var.azs[count.index] }"
		Group = var.stack
	}
}
