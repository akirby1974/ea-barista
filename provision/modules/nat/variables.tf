
variable "stack" {
	description = "The group name to tag the NAT resource with."
}

variable "azs" {
	description = "A list of availability zones to associate the NAT in."
	type        = list(string)
}

variable "public_subnet_ids" {
	description = "A list of public subnets to associate the NAT with."
	type        = list(string)
	default     = [] 
}
