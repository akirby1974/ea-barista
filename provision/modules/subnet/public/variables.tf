
/* Module variables */
variable "stack" {
	description = "The stack name to tag the public subnet with."
}

variable "vpc_id" {
	description = "The VPC to create the public subnet within."
}

variable "vpc_cidr_block" {
	description = "The VPC CIDR block to use when generating the public subnet CIDR block."
}

variable "cidr_offset" {
	description = "The offset to use when generating the public subnet CIDR block (default: 0)."
	default     = 0
}

variable "azs" {
	description = "A list of availability zones to create the public subnets within."
	type        = "list"
}

variable "purpose" {
	description = "A brief name describing the purpose of the public subnet(s) (default: proxy)."
	default     = "proxy"
}

variable "route_table_id" {
	description = "The routing table to associate with the public subnet(s)."
}

variable "network_acl_id" {
	description = "An existing network ACL to use for the public subnet(s)."
}

variable "network_acl_rules" {
	description = "A list of network ACLs to apply to the public subnet(s)."
	type        = "list"
	default     = []
}
