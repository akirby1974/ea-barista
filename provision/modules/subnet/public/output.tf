
/* Return the following values to the calling resource */
output "subnet_ids" {
	value = aws_subnet.module_subnet.*.id
}
