
/* Create the internet gateway for the VPC */
resource "aws_internet_gateway" "module_igw" {
	vpc_id = var.vpc_id
	
	tags = {
		Name  = "${ var.stack }.igw"
		Group = var.stack
	}
}

/* Update the default routing table */
resource "aws_default_route_table" "module_rtb" {
	default_route_table_id = var.route_table_id
	
	tags = {
		Name  = "${ var.stack }.rtb.${ var.purpose }"
		Group = var.stack
	}
}

/* Update the default VPC ACL */
resource "aws_default_network_acl" "module_default_network_acl" {
	default_network_acl_id = var.network_acl_id
	subnet_ids             = aws_subnet.module_subnet.*.id
	
	tags = {
		Name  = "${ var.stack }.netacl.${ var.purpose }"
		Group = var.stack
	}
}

/* Assign rules to the network acl */
resource "aws_network_acl_rule" "module_network_acl_rule" {
	count          = length(var.network_acl_rules)
	network_acl_id = aws_default_network_acl.module_default_network_acl.id
	
	egress         = lookup(var.network_acl_rules[count.index], "egress", false)
	rule_number    = lookup(var.network_acl_rules[count.index], "rule_no", 100)
	protocol       = lookup(var.network_acl_rules[count.index], "protocol", -1)
	rule_action    = lookup(var.network_acl_rules[count.index], "action", "deny")
	cidr_block     = lookup(var.network_acl_rules[count.index], "cidr_block", "0.0.0.0/0")
	from_port      = lookup(var.network_acl_rules[count.index], "from_port", 0)
	to_port        = lookup(var.network_acl_rules[count.index], "to_port", 0)
}

/* Create the public subnet */
resource "aws_subnet" "module_subnet" {
	count  = length(var.azs)
	vpc_id = var.vpc_id
	
	availability_zone = var.azs[count.index]
	cidr_block        = cidrsubnet(var.vpc_cidr_block, 8, count.index + (length(var.azs) * var.cidr_offset) + 1)
	
	tags = {
		Name  = "${ var.stack }.subnet.${ var.purpose }.${ var.azs[count.index] }"
		Group = var.stack
	}
}

/* Associate the public subnets with the routing tables */
resource "aws_route_table_association" "module_rtb_assoc" {
	count = length(var.azs)
	
	subnet_id      = element(aws_subnet.module_subnet.*.id, count.index)
	route_table_id = var.route_table_id
}
