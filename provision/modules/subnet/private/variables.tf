
variable "stack" {
	description = "The stack name to tag the private subnet with."
}

variable "vpc_id" {
	description = "The VPC to cretae the private subnet within."
}

variable "vpc_cidr_block" {
	description = "The CIDR block from the VPC."
}

variable "cidr_offset" {
	description = "An offset to start the private subnet CIDR block at based on the VPC CIDR block (default: 0)."
	default     = 0
}

variable "azs" {
	description = "A list of availability zones to create the private subnet(s) in."
	type        = "list"
}

variable "purpose" {
	description = "A short general name to describe the purpose of the private subnet(s)."
}

variable "route_table_ids" {
	description = "The routing tables to associate with the private subnet(s)."
	type        = "list"
}

variable "network_acl_rules" {
	description = "A list of network ACLs to associate with the private subnet(s)."
	type        = "list"
	default     = []
}
