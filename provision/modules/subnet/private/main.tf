
/* Create the private subnet */
resource "aws_subnet" "module_subnet" {
	count  = length(var.azs)
	vpc_id = var.vpc_id
	
	availability_zone = var.azs[count.index]
	cidr_block        = cidrsubnet(var.vpc_cidr_block, 8, count.index + (length(var.azs) * var.cidr_offset) + 1)
	
	tags = {
		Name  = "${ var.stack }.subnet.${ var.purpose }.${ var.azs[count.index] }"
		Group = var.stack
	}
}

/* Associate the private subnet with a routing table */
resource "aws_route_table_association" "module_rtb_assoc" {
	count = length(var.azs)
	
	subnet_id      = element(aws_subnet.module_subnet.*.id, count.index)
	route_table_id = var.route_table_ids[count.index]
}

/* Create the network ACL for the new subnets */
resource "aws_network_acl" "module_netacl" {
	vpc_id     = var.vpc_id
	subnet_ids = aws_subnet.module_subnet.*.id
	
	tags = {
		Name  = "${ var.stack }.netacl.${ var.purpose }"
		Group = var.stack
	}
}

/* Assign rules to the network acl */
resource "aws_network_acl_rule" "wd_network_acl_rule" {
	count          = length(var.network_acl_rules)
	network_acl_id = aws_network_acl.module_netacl.id
	
	egress         = lookup(var.network_acl_rules[count.index], "egress", false)
	rule_number    = lookup(var.network_acl_rules[count.index], "rule_no", 100)
	protocol       = lookup(var.network_acl_rules[count.index], "protocol", -1)
	rule_action    = lookup(var.network_acl_rules[count.index], "action", "deny")
	cidr_block     = lookup(var.network_acl_rules[count.index], "cidr_block", "0.0.0.0/0")
	from_port      = lookup(var.network_acl_rules[count.index], "from_port", 0)
	to_port        = lookup(var.network_acl_rules[count.index], "to_port", 0)
}
