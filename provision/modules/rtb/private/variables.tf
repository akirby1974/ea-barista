
variable "stack" {
	description = "The stack name to tag the routing table(s) with."
}

variable "vpc_id" {
	description = "The VPC to create the routing table in."
}

variable "azs" {
	description = "A list of availability zones to create the routing table(s) in."
	type        = list(string)
}

variable "purpose" {
	description = "A short name describing the purpose of the routing table(s)."
}

variable "nat_ids" {
	description = "A list of NATs to assign to the routing table(s)."
	type        = list(string)
	default     = []
}
