
/* Create the private routing table(s) */
resource "aws_route_table" "module_rtb" {
	count  = length(var.azs)
	vpc_id = var.vpc_id
	
	route {
		cidr_block     = "0.0.0.0/0"
		nat_gateway_id = var.nat_ids[count.index]
	}
	
	tags = {
		Name  = "${ var.stack }.rtb.${ var.purpose }.${ var.azs[count.index] }"
		Group = var.stack
	}
}
