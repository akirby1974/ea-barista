
/* Return the following values to the calling resource */
output "rtb_ids" {
	value = aws_route_table.module_rtb.*.id
}
