'use strict'

// load the database models and connect to the database
const db = require('../models');
const op = db.Sequelize.Op;

/**
 * Create a new Order in the queue.
 */
exports.postOrders = async (request, response) => {
    var packet = Object.assign({}, global.packet);

    try {
        // TODO: add code to validate the parameters

        // create the order header in the database
        packet.data = await db.Orders.create({
            coffeehouseId: request.body.coffeehouseId,
            customerId: request.body.customerId,
            pickup: request.body.pickup,
            status: 'Pending'
        }).then(function(record) {
            var data = record.dataValues;
            data.id = record.null;

            return data;
        });

        packet.message = '';
        packet.success = true
        packet.code = 200;
    } catch(error) {
        packet.message = error.message;
    }
    
    response.json(packet);
}

/**
 * Get a specified Order from the queue.
 */
exports.getOrder = async (request, response) => {
    var packet = Object.assign({}, global.packet);

    try {
        // get the path parameters
        var orderId = request.params.orderId;

        // get the specific record from the database
        packet.data = await db.Orders.findOne({
            where: {
                id: orderId
            }
        });

        packet.message = '';
        packet.success = true
        packet.code = 200;
    } catch(error) {
        packet.message = error.message;
    }
    
    response.json(packet);
}

/**
 * Update an existing Order in the queue.
 */
exports.putOrder = async (request, response) => {
    var packet = Object.assign({}, global.packet);

    try {
        // get the path parameters
        var orderId = request.params.orderId;

        // update the specified order header
        await db.Orders.update({
            pickup: request.body.pickup,
            status: request.body.status
        }, {
            where: {
                id: orderId
            }
        });

        packet.message = '';
        packet.success = true
        packet.code = 200;
    } catch(error) {
        packet.message = error.message;
    }
    
    response.json(packet);
}

/**
 * Delete an order from the queue.
 */
exports.deleteOrder = async (request, response) => {
    var packet = Object.assign({}, global.packet);

    try {
        // get the path parameters
        var orderId = request.params.orderId;

        // TODO: add code to validate the record can be deleted; completed orders can't be
        
        // delete the specified Order; cannot delete a completed order
        await db.Orders.destroy({
            where: {
                id: orderId,
                status: { [op.ne]: 'Completed' }
            }
        });

        packet.message = '';
        packet.success = true
        packet.code = 200;
    } catch(error) {
        packet.message = error.message;
    }
    
    response.json(packet);
}

/**
 * Get a list of Order Items for a specified Order.
 */
exports.getOrderItems = async (request, response) => {
    var packet = Object.assign({}, global.packet);

    try {
        // get the path parameters
        var orderId = request.params.orderId;

        // initialize the data payload
        packet.data = [];

        // ... put the functional code here ...

        packet.message = '';
        packet.success = true
        packet.code = 200;
    } catch(error) {
        packet.message = error.message;
    }
    
    response.json(packet);
}

/**
 * Add a new Order Item to a given Order.
 */
exports.postOrderItem = async (request, response) => {
    var packet = Object.assign({}, global.packet);

    try {
        // get the path parameters
        var orderId = request.params.orderId;

        // initialize the data payload
        packet.data = [];

        // ... put the functional code here ...

        packet.message = '';
        packet.success = true
        packet.code = 200;
    } catch(error) {
        packet.message = error.message;
    }
    
    response.json(packet);
}

/**
 * Get a specific Order Item.
 */
exports.getOrderItem = async (request, response) => {
    var packet = Object.assign({}, global.packet);

    try {
        // get the path parameters
        var orderId = request.params.orderId;
        var orderItemId = request.params.orderItemId;

        // initialize the data payload
        packet.data = [];

        // ... put the functional code here ...

        packet.message = '';
        packet.success = true
        packet.code = 200;
    } catch(error) {
        packet.message = error.message;
    }
    
    response.json(packet);
}

/**
 * Update an existing Order Item.
 */
exports.putOrderItem = async (request, response) => {
    var packet = Object.assign({}, global.packet);

    try {
        // get the path parameters
        var orderId = request.params.orderId;
        var orderItemId = request.params.orderItemId;

        // initialize the data payload
        packet.data = [];

        // ... put the functional code here ...

        packet.message = '';
        packet.success = true
        packet.code = 200;
    } catch(error) {
        packet.message = error.message;
    }
    
    response.json(packet);
}

/**
 * Delete an existing Order Item.
 */
exports.deleteOrderItem = async (request, response) => {
    var packet = Object.assign({}, global.packet);

    try {
        // get the path parameters
        var orderId = request.params.orderId;
        var orderItemId = request.params.orderItemId;

        // ... put the functional code here ...

        packet.message = '';
        packet.success = true
        packet.code = 200;
    } catch(error) {
        packet.message = error.message;
    }
    
    response.json(packet);
}
