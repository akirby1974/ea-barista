'use strict'

exports.send501 = function(request, response) {
    try {
        packet.message = 'An unknown resource/method has been called';
        packet.code = 501;
    } catch(error) {
        packet.message = error.message;
    }
    
    response.json(packet);
}
