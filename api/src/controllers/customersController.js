'use strict'

// load the database models and connect to the database
const db = require('../models');
const op = db.Sequelize.Op;

/**
 * Get a list of registered Customers.
 */
exports.getCustomers = async (request, response) => {
    var packet = Object.assign({}, global.packet);

    try {
        var limit = request.query.limit || 20;
        var offset = request.query.offset || 0;
        var active = String(request.query.active || 'true') == 'true';
        
        packet.data = await db.Customers.findAll({
            limit: limit,
            offset: offset,
            where: {
                active: active
            }
        });

        packet.message = '';
        packet.success = true
        packet.code = 200;
    } catch(error) {
        packet.message = error.message;
    }
    
    response.json(packet);
}

/**
 * Register a new Customer.
 */
exports.postCustomer = async (request, response) => {
    var packet = Object.assign({}, global.packet);

    try {
        // add the Customer record to the database
        packet.data = await db.Customers.create({
            name: request.body.name,
            email: request.body.email,
            password: request.body.password,   // TODO: encrypt this
            active: true
        }).then(function(record) {
            var data = record.dataValues;
            data.id = record.null;

            return data;
        });

        packet.message = '';
        packet.success = true
        packet.code = 200;
    } catch(error) {
        packet.message = error.message;
    }
    
    response.json(packet);
}

/**
 * Get a specific Customer.
 */
exports.getCustomer = async (request, response) => {
    var packet = Object.assign({}, global.packet);

    try {
        // get the path parameters
        var customerId = request.params.customerId;

        // get the data from the database
        packet.data = await db.Customers.findOne({
            where: {
                id: customerId
            }
        });

        packet.message = '';
        packet.success = true
        packet.code = 200;
    } catch(error) {
        packet.message = error.message;
    }
    
    response.json(packet);
}

/**
 * Update an existing Customer.
 */
exports.putCustomer = async (request, response) => {
    var packet = Object.assign({}, global.packet);

    try {
        // get the path parameters
        var customerId = request.params.customerId;

        await db.Customers.update({
            name: request.body.name,
            active: request.body.active
        }, {
            where: {
                id: customerId
            }
        });

        packet.data = await db.Customers.findOne({
            where: {
                id: customerId
            }
        });

        packet.message = '';
        packet.success = true
        packet.code = 200;
    } catch(error) {
        packet.message = error.message;
    }
    
    response.json(packet);
}

/**
 * Deactivate or delete an existing Customer.
 */
exports.deleteCustomer = async (request, response) => {
    var packet = Object.assign({}, global.packet);

    try {
        // get the path parameters
        var customerId = request.params.customerId;
        var purgeRecord = request.body.purge || false;

        if (purgeRecord) {
            // delete the Customer record from the database
            await db.Customers.destroy({
                where: {
                    id: customerId
                }
            });
        } else {
            // deactive the Customer record from the database
            await db.Customers.update({
                active: false
            }, {
                where: {
                    id: customerId
                }
            })
        }

        packet.message = '';
        packet.success = true
        packet.code = 200;
    } catch(error) {
        packet.message = error.message;
    }
    
    response.json(packet);
}

/**
 * Get a list of Orders for a specified Customer.
 */
exports.getCustomerOrders = async (request, response) => {
    var packet = Object.assign({}, global.packet);

    try {
        // get the path parameters
        var customerId = request.params.customerId;

        // TODO: add code to filter the results to pending orders (by status)

        // get the data from the database
        packet.data = await db.Orders.findAll({
            where: {
                customerId: customerId,
                status: { [op.ne]: 'Completed' }
            }
        });

        packet.message = '';
        packet.success = true
        packet.code = 200;
    } catch(error) {
        packet.message = error.message;
    }
    
    response.json(packet);
}
