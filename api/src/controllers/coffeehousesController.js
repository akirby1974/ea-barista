'use strict'

// load the database models and connect to the database
const db = require('../models');
const op = db.Sequelize.Op;

/**
 * Get a list of Coffeehouses in the system.
 */
exports.getCoffeehouses = async (request, response) => {
    var packet = Object.assign({}, global.packet);

    try {
        var limit = request.query.limit || 20;
        var offset = request.query.offset || 0;
        var active = String(request.query.active || 'true') == 'true';

        // retreive all the matching Coffeehouse records
        packet.data = await db.Coffeehouses.findAll({
            limit: limit,
            offset: offset,
            where: {
                active: active
            }
        });

        packet.message = '';
        packet.success = true
        packet.code = 200;
    } catch(error) {
        packet.message = error.message;
    }
    
    response.json(packet);
}

/**
 * Add a new Coffeehouse to the system.
 */
exports.postCoffeehouses = async (request, response) => {
    var packet = Object.assign({}, global.packet);

    try {
        // add the Coffeehouse record to the database
        packet.data = await db.Coffeehouses.create({
            name: request.body.name,
            address: request.body.address,
            hours: request.body.hours,
            email: request.body.email,
            password: request.body.password,   // TODO: encrypt this
            active: true
        }).then(function(record) {
            var data = record.dataValues;
            data.id = record.null;

            return data;
        });

        packet.message = '';
        packet.success = true
        packet.code = 200;
    } catch(error) {
        packet.message = error.message;
    }

    response.json(packet);
}

/**
 * Get a specific Coffeehouse.
 */
exports.getCoffeehouse = async (request, response) => {
    var packet = Object.assign({}, global.packet);

    try {
        // get the path parameters
        var coffeehouseId = request.params.coffeehouseId;

        // get the data from the database
        packet.data = await db.Coffeehouses.findOne({
            where: {
                id: coffeehouseId
            }
        });

        packet.message = '';
        packet.success = true
        packet.code = 200;
    } catch(error) {
        packet.message = error.message;
    }
    
    response.json(packet);
}

/**
 * Update an existing Coffeehouse.
 */
exports.putCoffeehouse = async (request, response) => {
    var packet = Object.assign({}, global.packet);

    try {
        // get the path parameters
        var coffeehouseId = request.params.coffeehouseId;

        // initialize the data payload
        packet.data = [];

        // update the reocrd
        await db.Coffeehouses.update({
            name: request.body.name,
            address: request.body.address,
            hours: request.body.hours,
            active: request.body.active
        }, {
            where: {
                id: coffeehouseId
            }
        });

        // get the latest information stored at the specified id
        packet.data = await db.Coffeehouses.findOne({
            where: {
                id: coffeehouseId
            }
        })

        packet.message = '';
        packet.success = true
        packet.code = 200;
    } catch(error) {
        packet.message = error.message;
    }
    
    response.json(packet);
}

/**
 * Deactivate or delete an existing Coffeehouse.
 */
exports.deleteCoffeehouse = async (request, response) => {
    var packet = Object.assign({}, global.packet);

    try {
        // get the path parameters
        var coffeehouseId = request.params.coffeehouseId;
        var purgeRecord = request.body.purge || false;

        if (purgeRecord) {
            // delete the Coffeehouse record from the database
            await db.Coffeehouses.destroy({
                where: {
                    id: coffeehouseId
                }
            });
        } else {
            // deactive the Coffeehouse record from the database
            await db.Coffeehouses.update({
                active: false
            }, {
                where: {
                    id: coffeehouseId
                }
            })
        }

        packet.message = '';
        packet.success = true
        packet.code = 200;
    } catch(error) {
        packet.message = error.message;
    }
    
    response.json(packet);
}

/**
 * Get all Orders for the specified Coffeehouse
 */
exports.getCoffeehouseOrders = async (request, response) => {
    var packet = Object.assign({}, global.packet);

    try {
        // get the path parameters
        var coffeehouseId = request.params.coffeehouseId;

        // TODO: add code to filter the results to pending orders (by status)
        // TODO: add code to order by pickup time

        packet.data = await db.Orders.findAll({
            where: {
                coffeehouseId: coffeehouseId,
                status: { [op.ne]: 'Completed' }
            },
            order: [
                ['pickup', 'ASC']
            ]
        });

        packet.message = '';
        packet.success = true
        packet.code = 200;
    } catch(error) {
        packet.message = error.message;
    }
    
    response.json(packet);
}

