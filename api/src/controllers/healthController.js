'use strict'

exports.getHealth = function(request, response) {
    var packet = Object.assign({}, global.packet);

    try {
        packet.success = true;
        packet.message = 'Service is running';
        packet.code = 200;
    } catch(error) {
        packet.message = error.message;
    }

    response.json(packet);
}
