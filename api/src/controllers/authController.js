'use strict'

// load the database models and connect to the database
const db = require('../models');

/**
 * Authenticate the Coffeehouse Barista or Customer
 */
exports.postAuth = function(request, response) {
    try {

        // ... put the functional code here ...

        packet.message = '';
        packet.success = true
        packet.code = 200;
    } catch(error) {
        packet.message = error.message;
    }

    response.json(packet);
}
