'use strict'

// declare the needed variables to drive the application
const express = require('express'),
    app = express(),
    port = process.env.NODE_PORT || 8500,
    bodyParser = require('body-parser');

// setup the default packet object
global.packet = {
    success: false,
    message: "Unknown error has occured.",
    code: 500
};

app.use(function(req, res, next) {
    // build up the API response header
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");

    res.header(
        "Access-Control-Allow-Headers",
        "Content-type,Accept,x-access-token,X-Key"
    );

    if (req.method == "OPTIONS") {
        res.status(200).end();
    } else {
        next();
    }
});
    
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// handle routing of the application
var route = require('./routes')
route(app);

// handle any requests that can't be routed
app.get('*', function(request, response) {
    var error = require('./controllers/errorController');
    error.send501(request, response);
});

// start listening for requests
app.listen(port, () => {
    console.log('Server started on port ', port);
});
