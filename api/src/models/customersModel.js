'use strict';

module.exports = (sequelize, DataTypes) => {
    const Customers = sequelize.define('Customers', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        name: {
            type: DataTypes.STRING(255),
        },
        email: {
            type: DataTypes.STRING(255),
            allowNull: false,
            validate: {
              notNull: true,
              isEmail: true
            }
        },
        password: {
            type: DataTypes.STRING(255),
            allowNull: false,
            validate: {
              notNull: true
            }
        },
        active: {
            type: DataTypes.BOOLEAN,
        }
      }, {});

    Customers.associate = function(models) {
        // associations can be defined here
    };

    return Customers;
};