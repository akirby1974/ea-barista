'use strict';

// load the application configuration file based on environment
const env = process.env.NODE_ENV || 'development';
const config = require(__dirname + '/../config/database.json')[env];

// initialize file system variables
const fs = require('fs');
const path = require('path');
const basename = path.basename(__filename);

// declare database handling variables
const Sequelize = require('sequelize');
const db = {};

let sequelize;

// create a connection to the specified database (found in the config file)
if (config.use_env_variable) {
    sequelize = new Sequelize(process.env[config.use_env_variable], config);
} else {
    // handle connecting differently for sqlite
    if (config.dialect === 'sqlite') {
        sequelize = new Sequelize('sqlite:' + path.resolve(__dirname, '../' + config.storage));
    } else {
        sequelize = new Sequelize(config.database, config.username, config.password, config);
    }
}

// dynamcially load in the data models
fs
    .readdirSync(__dirname)
    .filter(file => {
        return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
    })
    .forEach(file => {
        const model = sequelize['import'](path.join(__dirname, file));
        db[model.name] = model;
    });

Object.keys(db).forEach(modelName => {
    if (db[modelName].associate) {
        db[modelName].associate(db);
    }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
