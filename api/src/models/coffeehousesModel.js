'use strict';

module.exports = (sequelize, DataTypes) => {
    const Coffeehouses = sequelize.define('Coffeehouses', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        name: {
            type: DataTypes.STRING(255),
        },
        address: {
            type: DataTypes.STRING(255),
        },
        hours: {
            type: DataTypes.STRING(255),
        },
        email: {
            type: DataTypes.STRING(255),
            allowNull: false,
            validate: {
              notNull: true,
              isEmail: true
            }
        },
        password: {
            type: DataTypes.STRING(255),
            allowNull: false,
            validate: {
              notNull: true
            }
        },
        active: {
            type: DataTypes.BOOLEAN,
        }
    }, {});

    Coffeehouses.associate = function(models) {
        // associations can be defined here
    };

    return Coffeehouses;
};
