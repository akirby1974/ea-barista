'use strict';

module.exports = (sequelize, DataTypes) => {
    const Orders = sequelize.define('Orders', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        coffeehouseId: {
          type: DataTypes.INTEGER,
        },
        customerId: {
          type: DataTypes.INTEGER,
        },
        pickup: {
          type: DataTypes.STRING(15),
        },
        status: {
          type: DataTypes.STRING(15),
        }
    }, {});

    Orders.associate = function(models) {
        // associations can be defined here
    };

    return Orders;
};