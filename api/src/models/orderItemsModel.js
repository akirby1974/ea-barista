'use strict';

module.exports = (sequelize, DataTypes) => {
    const OrderItems = sequelize.define('OrderItems', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        orderId: {
            type: DataTypes.INTEGER,
        },
        description: {
            type: DataTypes.STRING(255),
        },
        quantity: {
            type: DataTypes.INTEGER,
        },
        cost: {
            type: DataTypes.DECIMAL,
        }
    }, {});

    OrderItems.associate = function(models) {
        // associations can be defined here
    };

    return OrderItems;
};