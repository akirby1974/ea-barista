'use strict'

module.exports = function(app) {
    // import the globla error controller
    var error = require('./controllers/errorController');

    //
    // APPLICATION AUTHENTICATION API END-POINT
    //

    // import the authentication controller
    var auth = require('./controllers/authController');
    app.route('/auth')
        .post(auth.postAuth)
        .all(error.send501);
    
    
    //
    // COFFEEHOUSE API END-POINTS
    //

    // import the coffeehouses controller
    var coffeehouses = require('./controllers/coffeehousesController');
    app.route('/coffeehouses')
        .get(coffeehouses.getCoffeehouses)
        .post(coffeehouses.postCoffeehouses)
        .all(error.send501);

    app.route('/coffeehouses/:coffeehouseId')
        .get(coffeehouses.getCoffeehouse)
        .put(coffeehouses.putCoffeehouse)
        .delete(coffeehouses.deleteCoffeehouse)
        .all(error.send501);

    app.route('/coffeehouses/:coffeehouseId/orders')
        .get(coffeehouses.getCoffeehouseOrders)
        .all(error.send501);

    //
    // CUSTOMER API END-POINTS
    //
    
    // import the customers controller
    var customers = require('./controllers/customersController');
    app.route('/customers')
        .get(customers.getCustomers)
        .post(customers.postCustomer)
        .all(error.send501);

    app.route('/customers/:customerId')
        .get(customers.getCustomer)
        .put(customers.putCustomer)
        .delete(customers.deleteCustomer)
        .all(error.send501);
    
    app.route('/customers/:customerId/orders')
        .get(customers.getCustomerOrders)
        .all(error.send501);


    //
    // HEALTH END-POINT
    //

    // import the health controller
    var health = require('./controllers/healthController');
    app.route('/health')
        .get(health.getHealth)
        .all(error.send501);


    //
    // ORDER AND ORDER ITEM API END-POINTS
    //
    
    // import the orders controller
    var orders = require('./controllers/ordersController');
    app.route('/orders')
        .post(orders.postOrders)
        .all(error.send501);
    
    app.route('/orders/:orderId')
        .get(orders.getOrder)
        .put(orders.putOrder)
        .delete(orders.deleteOrder)
        .all(error.send501);
    
    app.route('/orders/:orderId/items')
        .get(orders.getOrderItems)
        .post(orders.postOrderItem)
        .all(error.send501);

    app.route('/orders/:orderId/items/:orderItemId')
        .get(orders.getOrderItem)
        .put(orders.putOrderItem)
        .delete(orders.deleteOrderItem)
        .all(error.send501);
};
