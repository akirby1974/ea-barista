#!/bin/bash

docker pull swaggerapi/swagger-ui
docker run -p 80:8080 -e SWAGGER_JSON=/api/barista.yaml -v $PWD/spec:/api swaggerapi/swagger-ui
