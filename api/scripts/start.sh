#!/bin/bash

# source the common.rc file to set commonly used variables
SCRIPTS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source $SCRIPTS/common.rc

if [ "$BARISTA_IMAGE_ID" = "" ]; then
	echo "you need to run 'make build' to build the image first"
    exit 1
fi

if [ "$BARISTA_CONT_ID" = "" ] && [ "$BARISTA_IMAGE_ID" != "" ]; then
    docker run -d --name $BARISTA_CONTRAINER_NAME -p $NODE_PORT:8500 $BARISTA_IMAGE_VOLUME $BARISTA_DOCKER_IMAGE:latest
fi
