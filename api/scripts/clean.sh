#!/bin/bash

# source the common.rc file to set commonly used variables
SCRIPTS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source $SCRIPTS/common.rc

if [ "$BARISTA_CONT_ID" = "" ] && [ "$BARISTA_IMAGE_ID" != "" ]; then
    docker container prune -f
    docker image rm -f $BARISTA_DOCKER_IMAGE
fi
