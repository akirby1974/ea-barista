# Barista Assignment

## Introduction

The content within this folder is everything related to the Barista's APIs; API Contact (OpenAPI), the API source code, and files to support the build/deploy.

**API Contract**

You can find the API Contract below the `spec/` folder.  It's written in OpenAPI (Swagger v3) using the Swagger Editor and Viewer.  To view the API Contract, you can either use Make, the shell script or do it manually.

In addition to the API Contract, you will find a Postman Collection of test end-points to validate the APIs.  Import the file into your Postman client, start up the APIs either natively or in a container (see below for instructions) and start testing the API end-points.

**Using Make:**

You can use the Makefile to start up the Swagger UI Docker container with the Coffeehouse API contract:

```
$> make swagger
```

**Using the script:**

You can also use the shell script `scripts/swagger.sh` to do the same thing as Make:

```
$> ./scripts/swagger.sh
```

**Using the manual approach:**

To start up the Swagger UI docker container to view the Coffeehouse API Contract, follow these steps:

```
$> docker pull swaggerapi/swagger-ui
$> docker run -p 80:8080 -e SWAGGER_JSON=/api/barista.yaml -v $(PWD)/spec:/api swaggerapi/swagger-ui
```

**API Source Code**

The API source code can be found under the `src/` folder and the server bootstrap and routing files are found at the top level folder.  The `src/` folder contains the following sub-folders and files:

| FOLDER/FILE  | DESCRIPTION                                                                     |
|:-------------|:--------------------------------------------------------------------------------|
| config/      | The configuration files; eg. database connection.                               |
| controllers/ | The controller files where the business logic of each API end-port can found.   |
| migrations/  | The database migration files for building up the database and table structures. |
| models/      | The data models used for interfacing the APIs with the database.                |
| seeders/     | Contains files that can be used for populating a test database.                 |
| routes.js    | This file handles where to route requests.                                      |
| server.js    | This is the server's main bootstrap interface to get the show started.          |

## Requirements

At minimum you will need to install the following:

* Docker

If you want to run this **natively** in your sandbox, you will need the following as well:

* Node
* GNU Make *(only to use make to automate build/deploy)*

## Development

This section will walk you through standing up the APIs either natively in your sandbox or as a Docker container.  Before you standup your sandbox APIs, you will first need to configure our environment.

### Configure Your Environment

There is an environment template file at the can be used to setup your environment.

```
$> cp env.rc.template env.rc
```

Edit the file to suite your environment.  Before starting the Node server natively on your machine you will need to source this file.

```
$> source env.rc
```

If you are going to run this natively on your machine, you will need to install the Node packages.  You can do this by running the following:

```
$> npm install
```

### Standup Native Sandbox

This section walks you through the steps to bring up the Coffeehouse's APIs natively on your machine.

```
$> source env.rc
$> npm start
```

The Node package.json file is setup to conditionally start the server either with node (non-development) or nodemon (development).  If you want to bring up one or the other specifically, you can use the following:

**For development:**

```
$> npm run start-dev
```

**For non-development:**

```
$> npm run start-prod
```

**NOTE:** Regardless of starting the Node server up with `start-dev` or `start-prod`, the server will still look at `NODE_ENV` to determine what environment the server is running in.  The only thing you are controlling is if the Node server is started with `node` or `nodemon`.

### Standup Container Sandbox

A Makefile has been created at the root of the API folder that automates a lot of this work for you.  If you have Make installed on your machine, you will have the following commands available to you.  If you don't have Make installed, you can skip this **Standup Container with Make** section and scroll down to **Standup Container with Script** or down to **Standup Container Manually**.

Once the container is running, you should be able to point your browser to the following URL to check on the health of the APIs:

```
http://localhost:8500/health
```

This is assuming you are using the default `NODE_PORT`.  If not, the port `8500` above should be replaced with the port you have specified on the `NODE_PORT` environment variable.

The output from above should look something like the following:

```json
{
    "success":true,
    "message":"Service is running",
    "code":200
}
```

**Standup Container with Make:**

The following section assumes you have Make installed to automate the building and clean up of the container builds as well as standing them up locally on your machine.  The following table lists the commands available and a description of each:

| Make Command | Description |
|:-------------|:------------|
| build        | Builds the Docker image containing the API source code.   |
| clean        | Removes any previously built Docker images.               |
| start        | Starts the latest built Docker image.  Depending on `NODE_ENV`, it will mount your local sandbox as a volume in the container or use the copied source code in the image.   |
| stop         | Stops the prunes the container from your environment.     |
| attach       | Logs you into the running container through SSH.          |
| setup        | Creates the database defined in `src/config/database.json` based on your `NODE_ENV` and seeds it. **NOT IMPLEMENTED** |
| migrate      | Drops the existing database defined in `src/config/database.json` based on your `NODE_ENV` and recreates it before seeding it. **NOT IMPLEMENTED** |
| seed         | Seeds the existing database defined in `src/config/database.json` based on your `NODE_ENV`. **NOT IMPLEMENTED** |

To get things stood up, run the following:

```
$> source env.rc
$> make build
$> make start
```

To clean things up, run the following:

```
$> make clean
```

This will stop any running containers, before cleaning up the container and the image from your machine.

If you want to checkout things within your container, you can run the following:

```
$> make attach
```

**NOTE:** The other commands; setup, migrate and seed are not fully implemented yet and at the time of writing this would only be used for setting up the database for a natively stood up build.

**Standup Container with Scripts:**

If you don't want to install Make on your machine, you can use the scripts under the `scripts/` folder.  The following table describes each script:

| Script Name | Description |
|:------------|:------------|
| build.sh    | Builds the Docker image.                               |
| clean.sh    | Removes any traces of the Docker image and containers. |
| start.sh    | Starts an already built Docker image.                  |
| stop.sh     | Stops any running Docker containers.                   |

The above scripts are fairly straightforward; use the `build.sh` to build and re-build the Docker image, use the `clean.sh` to clean up your environment from previous build and runs of the Docker image, use `start.sh` get start the Docker container and use `stop.sh` to stop any running Docker containers.

**Standup Container Manually:**

If you don't want to use the automation in place and prefer more manual way of doing things, you can follow the steps in this section to build, deploy and teardown a locally running container.

To build the Docker image, run the following:

```
$> docker build -t $USER/barista_apis_image . --build-arg BUILD_ENV=$NODE_ENV
```

To start an already built image:

```
$> docker run -d --name barista_apis -p 8500:8500 $USER/barista_apis_image:latest
```

If you want to mount your local sandbox as a volume in the running container, run this command instead of the above:

```
$> docker run -d --name barista_apis -p 8500:8500 -v $PWD:/app $USER/barista_apis_image:latest
```

To stop and clean things up:

```
$> docker stop barista_apis
$> docker container prune -f
$> docker image rm -f $USER/barista_apis_image
```

## Build and Deploy

This section would include information on how to build the Barista Coffeehouse APIs for production and deploy the image into a K8s environment like EKS, ECS, AKS, etc..
